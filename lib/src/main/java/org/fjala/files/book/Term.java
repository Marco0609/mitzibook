package org.fjala.files.book;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class Term<T extends Comparable> {

	private String name;
	private List<T> elements;
	private boolean isListOfPages;

	public Term(String name, T element, boolean isListOfPages) {
		this.name = name;
		this.isListOfPages = isListOfPages;
		elements = new ArrayList<T>();
		elements.add(element);
	}

	public Term(String name, List<T> elements) {
		this.name = name;
		this.elements = elements;
	}

	public String getName() {
		return name;
	}

	public void addElement(T element) {
		elements.add(element);
	}

	public List<T>getPages() {
		return elements;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Term)) return false;
		Term<?> term = (Term<?>) o;
		return isListOfPages == term.isListOfPages && getName().equals(term.getName()) && elements.equals(term.elements);
	}

	@Override
	public int hashCode() {
		return Objects.hash(getName(), elements, isListOfPages);
	}

	private String pagesToString() {
		Collections.sort(elements);

		if (isListOfPages) {
			int previousPage = (Integer) elements.get(0);
			int countPages = 0;
			StringBuilder pagesSorted = new StringBuilder(String.valueOf(previousPage));

			for (int i = 1; i < elements.size(); i++) {
				int currentPage = (Integer) elements.get(i);
				if (currentPage - previousPage > 1) {
					if (countPages > 0) {
						pagesSorted.append(" - ");
						pagesSorted.append(previousPage);
					}
					pagesSorted.append(", ");
					pagesSorted.append(currentPage);

					countPages = 0;
				} else {
					countPages++;
				}
				previousPage = currentPage;
			}

			if (countPages > 0) {
				pagesSorted.append(" - ");
				pagesSorted.append(previousPage);
			}
			return pagesSorted.toString();
		} else {
			StringBuilder definitionSorted = new StringBuilder();

			for (int i = 0; i < elements.size(); i++) {
				definitionSorted.append(elements.get(i));
				definitionSorted.append("\n");
			}
			return definitionSorted.toString().substring(0, (definitionSorted.length() - 1));

		}
	}
	
	@Override
	public String toString() {
		return name + ": " + pagesToString();
		
	}
}
