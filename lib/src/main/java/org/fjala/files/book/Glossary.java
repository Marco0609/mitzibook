package org.fjala.files.book;

import org.fjala.files.redblacktree.RedBlackNode;

public class Glossary extends Dictionary {
    public void addTerm(String name, String definition) {
        RedBlackNode<Term, String> termNode = terms.get(name);
        if (termNode != null) {
            Term term = termNode.getValue();
            term.addElement(definition);
        } else {
            Term term = new Term(name, definition, false);
            terms.insert(term, name);
        }
    }
}
