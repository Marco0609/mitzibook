package org.fjala.files.book;

public class Book {

	private String name;
	private BookIndex index;
	private Glossary glossary;

	public Book(String name) {
		this.name = name;
		this.index = new BookIndex();
		this.glossary = new Glossary();
	}

	public String getName() {
		return name;
	}

	public BookIndex getIndex() {
		return index;
	}

	public void setIndex(BookIndex index) {
		this.index = index;
	}

	public Glossary getGlossary() {
		return glossary;
	}

	public void setGlossary(Glossary glossary) {
		this.glossary = glossary;
	}

	public void printBook() {
		System.out.println(name);
		index.printTerms();
	}
}
