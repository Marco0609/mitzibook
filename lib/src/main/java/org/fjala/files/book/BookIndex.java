package org.fjala.files.book;
import org.fjala.files.redblacktree.RedBlackNode;

public class BookIndex extends Dictionary {

    public void addTerm(String name, int page) {
        RedBlackNode<Term, String> termNode = terms.get(name);
        if (termNode != null) {
            Term term = termNode.getValue();
            term.addElement(page);
        } else {
            Term term = new Term(name, page, true);
            terms.insert(term, name);
        }
    }

}
