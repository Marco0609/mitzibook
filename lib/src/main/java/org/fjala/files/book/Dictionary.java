package org.fjala.files.book;

import org.fjala.files.redblacktree.RedBlackNode;
import org.fjala.files.redblacktree.RedBlackTree;

import java.util.ArrayList;
import java.util.List;

public class Dictionary {

    protected RedBlackTree<Term, String> terms;
    private static final RedBlackNode<Term, String> NIL = new RedBlackNode<>(null, null, null);

    public Dictionary() {
        terms = new RedBlackTree<>(NIL);
    }

    public void deleteTerm(String name) {
        terms.delete(name);
    }

    public void updateTerm(String oldName, String newName) {
        RedBlackNode<Term, String> oldNode = terms.get(oldName);
        if (oldNode != null) {
            Term oldTerm = oldNode.getValue();
            Term newTerm = new Term(newName, oldTerm.getPages());
            terms.delete(oldName);
            terms.insert(newTerm, newName);
        }
    }

    public String printTerms() {
        List<Term> termValues = terms.inorder();
        StringBuilder terms = new StringBuilder();
        for (Term termValue : termValues) {
            terms.append(termValue.toString());
            terms.append("\n");
        }
        return terms.toString().substring(0, (terms.length() - 1));
    }

    public List<Term> searchWordStartsWith(String word) {
        List<Term> termValues = terms.inorder();
        List<Term> termsBeginningWith = new ArrayList<>();
        for (Term termValue: termValues) {
            if (termValue.getName().startsWith(word)) {
                termsBeginningWith.add(termValue);
            }
        }
        return termsBeginningWith;
    }
}
