package org.fjala.files;

import org.fjala.files.book.Book;
import org.fjala.files.book.BookIndex;
import org.fjala.files.book.Glossary;

import org.fjala.files.book.Term;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class BookTest {

    private static Glossary glossary;
    private static BookIndex index;
    private static Book book;

    @BeforeAll
    private static void initializeGlossaryAndBookIndex() {
        glossary = new Glossary();
        glossary.addTerm("Atmosphere", "The gaseous mass that surrounds any planet, including Earth.");
        glossary.addTerm("Density", "The number (as of particles) per unit of measure.");
        glossary.addTerm("Galaxy", "A system of stars independent from all other systems");
        glossary.addTerm("Moon", "The natural satellite of any planet");
        glossary.addTerm("Orbit", "The path taken by a satellite around a celestial body.");
        glossary.addTerm("Solar", "Having to do with the sun.");

        index = new BookIndex();
        index.addTerm("Arrays", 20);
        index.addTerm("Binary Search", 30);
        index.addTerm("Binary Search Trees", 40);
        index.addTerm("Child Nodes", 50);

        book = new Book("Tests");
        book.setGlossary(glossary);
        book.setIndex(index);
    }

    @Test
    void addTermsToTheGlossaryAndIndexThatAlreadyExists() {
        book.getGlossary().addTerm("Moon", "Celestial body that revolves around a planet.");
        String actualResult = book.getGlossary().printTerms();
        String expectedResult = "Atmosphere: The gaseous mass that surrounds any planet, including Earth.\n" +
                "Density: The number (as of particles) per unit of measure.\n" +
                "Galaxy: A system of stars independent from all other systems\n" +
                "Moon: Celestial body that revolves around a planet.\n" +
                "The natural satellite of any planet\n" +
                "Orbit: The path taken by a satellite around a celestial body.\n" +
                "Solar: Having to do with the sun.";

        assertThat(actualResult, equalTo(expectedResult));

        book.getIndex().addTerm("Binary Search", 31);
        book.getIndex().addTerm("Binary Search", 100);

        actualResult = book.getIndex().printTerms();
        expectedResult = "Arrays: 20\n" +
                "Binary Search: 30 - 31, 100\n" +
                "Binary Search Trees: 40\n" +
                "Child Nodes: 50";

        assertThat(actualResult, equalTo(expectedResult));
    }

    @Test
    void deleteATermFromTheIndexAndGlossaryShouldReturnAStringWithoutTheDeletedTerm() {
        book.getGlossary().deleteTerm("Moon");
        String actualResult = book.getGlossary().printTerms();
        String expectedResult = "Atmosphere: The gaseous mass that surrounds any planet, including Earth.\n" +
                "Density: The number (as of particles) per unit of measure.\n" +
                "Galaxy: A system of stars independent from all other systems\n" +
                "Orbit: The path taken by a satellite around a celestial body.\n" +
                "Solar: Having to do with the sun.";

        assertThat(actualResult, equalTo(expectedResult));

        book.getIndex().deleteTerm("Arrays");
        actualResult = book.getIndex().printTerms();
        expectedResult = "Binary Search: 30\n" +
                "Binary Search Trees: 40\n" +
                "Child Nodes: 50";

        assertThat(actualResult, equalTo(expectedResult));
    }

    @Test
    void getAllTheTermsThatStartWhitAWordShouldReturnAListThatContainsAllTermsFound() {
        List<Term> actualTerms = book.getGlossary().searchWordStartsWith("Orb");
        List<Term> expectedTerms = new ArrayList();
        expectedTerms.add(new Term("Orbit", "The path taken by a satellite around a celestial body.", false));

        assertThat(actualTerms, equalTo(expectedTerms));

        expectedTerms.clear();
        actualTerms.clear();
        actualTerms = book.getIndex().searchWordStartsWith("Bin");
        expectedTerms.add(new Term("Binary Search", 30, true));
        expectedTerms.add(new Term("Binary Search Trees", 40, true));

        assertThat(actualTerms, equalTo(expectedTerms));
    }
}
